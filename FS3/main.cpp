#include <sstream>
#include "DBManager.h"

void ReadStudentsData(string data, string hash, string tree, DBManager* db);
void ReadProfessorsData(string data, string hash, string tree, DBManager* db);
void ReadQueriesData(string filename, DBManager* db);

void main() {
	// Students data
	string Stud_Hash_File	= "Students.hash";
	string Stud_Score_File	= "Students_Score.idx";
	string Stud_Data_File	= "student_data.csv";
	string Stud_DataBase	= "Students.DB";
	// Professors data
	string Prof_Hash_File	= "Professors.hash";
	string Prof_Salary_File	= "Professors_Salary.idx";
	string Prof_Data_File	= "prof_data.csv";
	string Prof_DataBase	= "Professors.DB";
	// Queries data
	string Query_In			= "query.dat";
	string Query_Out		= "query.res";
	// DB initialization
	DBManager* db = new DBManager(Stud_DataBase, Prof_DataBase, Query_Out);

	// Parse data
	ReadStudentsData(Stud_Data_File, Stud_Hash_File, Stud_Score_File, db);
	ReadProfessorsData(Prof_Data_File, Prof_Hash_File, Prof_Salary_File, db);
	ReadQueriesData(Query_In, db);

	//db->PrintHashTable();
	/*int node = 0;
	while (node != -1) {
		cout << endl << "Choose data to print node(Student: s / Professor: p): ";
		char table;
		cin >> table;
		cout << endl << "Node of B+-tree to print out: ";
		unsigned node;
		cin >> node;
		db->PrintKthTreeNode(table, node);
	}*/
	delete db;
}

void ReadStudentsData(string data, string hash, string tree, DBManager * db) {
	cout << "Inserting students data into DB..." << endl;

	ifstream DataFile(data, ios_base::in); //open file for reading
	if (DataFile.is_open()) {
		unsigned recNum;			// number of records
		string line, temp;			// strings for storing temporary values
		string studName;
		unsigned studID, advID;
		float score;

		getline(DataFile, line);
		recNum = stoi(line);

		db->InstantiateStudentDB(hash, tree, recNum);

		getline(DataFile, line);
		stringstream ss(line);

		// reading values line by line and insert them into DB
		for (unsigned i = 0; i < recNum; i++) {
			getline(ss, studName, ',');
			getline(ss, temp, ',');
			studID = stoi(temp);
			getline(ss, temp, ',');
			score = stof(temp);
			getline(ss, temp);
			advID = stoi(temp);

			db->InsertStudRecord(studID, studName, score, advID);

			getline(DataFile, line);
			ss.str(string());
			ss.clear();
			ss << line;
		}
		DataFile.close();
	}
}

void ReadProfessorsData(string data, string hash, string tree, DBManager * db) {
	cout << "Inserting professors data into DB..." << endl;

	ifstream DataFile(data, ios_base::in); //open file for reading
	if (DataFile.is_open()) {
		unsigned recNum;			// number of records
		string line, temp;			// strings for storing temporary values
		string profName;
		unsigned profID;
		int salary;

		getline(DataFile, line);
		recNum = stoi(line);

		db->InstantiateProfessorDB(hash, tree, recNum);

		getline(DataFile, line);
		stringstream ss(line);

		// reading values line by line and insert them into DB
		for (unsigned i = 0; i < recNum; i++) {
			getline(ss, profName, ',');
			getline(ss, temp, ',');
			profID = stoi(temp);
			getline(ss, temp);
			salary = stoi(temp);

			db->InsertProfRecord(profID, profName, salary);

			getline(DataFile, line);
			ss.str(string());
			ss.clear();
			ss << line;
		}
		DataFile.close();
	}
}

void ReadQueriesData(string filename, DBManager * db) {
	cout << "Executing queries: ";

	ifstream DataFile(filename, ios_base::in); //open file for reading
	if (DataFile.is_open()) {
		unsigned recNum;			// number of records
		string line, temp;			// strings for storing temporary values
		string args[3];

		getline(DataFile, line);
		recNum = stoi(line);

		getline(DataFile, line);
		stringstream ss(line);

		for (unsigned i = 0; i < recNum; i++) {
			getline(ss, args[0], ',');
			getline(ss, args[1], ',');

			if (!args[0].compare("Search-Range")) {
				cout << "Range search, ";
				getline(ss, args[2], ',');

				if (!args[1].compare("Students")) {
					float min, max;
					getline(ss, temp, ',');
					min = stof(temp);
					getline(ss, temp);
					max = stof(temp);
					db->ExecuteRangeQuery(stud, score, 0, 0, min, max);
				}
				else {
					int min, max;
					getline(ss, temp, ',');
					min = stoi(temp);
					getline(ss, temp);
					max = stoi(temp);
					db->ExecuteRangeQuery(prof, salary, min, max, 0, 0);
				}
			}

			else if (!args[0].compare("Search-Exact")) {
				cout << "Exact search, ";
				getline(ss, args[2], ',');
				getline(ss, temp);
				unsigned exactID = stoi(temp);

				if (!args[1].compare("Students"))
					db->ExecuteExactQuery(stud, studID, exactID);
				else
					db->ExecuteExactQuery(prof, profID, exactID);
			}
			else {
				cout << "Join, ";
				db->ExecuteJoin();
			}

			getline(DataFile, line);
			ss.str(string());
			ss.clear();
			ss << line;
		}
		DataFile.close();
	}
	cout << endl;
}
