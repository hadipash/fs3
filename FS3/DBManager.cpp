﻿#include "DBManager.h"

DBManager::DBManager(string sdb, string pdb, string q) {
	Stud_DataBase.open(sdb, ios_base::in | ios_base::out | ios_base::trunc | ios_base::binary);
	Prof_DataBase.open(pdb, ios_base::in | ios_base::out | ios_base::trunc | ios_base::binary);
	Query.open(q, ios_base::out | ios_base::trunc);

	// Allocate DB files with initial two blocks
	if (Stud_DataBase.is_open()) {
		Stud_DataBase.seekp((1 << 13) - 1);	// 8096 bytes
		Stud_DataBase.write("", 1);
	}
	if (Prof_DataBase.is_open()) {
		Prof_DataBase.seekp((1 << 13) - 1);	// 8096 bytes
		Prof_DataBase.write("", 1);
	}

}

DBManager::~DBManager() {
	cout << "Freeing memory and saving files..." << endl;
	Stud_DataBase.close();
	Prof_DataBase.close();
	Query.close();

	delete studHash;
	delete profHash;
	delete studTree;
	delete profTree;
}

void DBManager::InstantiateStudentDB(string hashfile, string score, unsigned n) {
	studHash = new Hash((n / bfStud) * 2, hashfile, bfStud); // Suppose that the most worst utilization of a block is 1/2 and key values evenly distributed
	studTree = new BpTree<float>(score);
}

void DBManager::InsertStudRecord(unsigned ID, string name, float score, unsigned advID) {
	bool update = false;	// whether need to read and insert again records from an overflowed block or not
	unsigned offset = 0;	// position of a recond in a block (to speed-up inserting)
	unsigned blockNum = studHash->Hashing(ID, &update, &offset);	// insert a key value into tha hash table and return a block number

	if (update) {
		UpdateStud(blockNum);						// Re-insert records from an overflowed block
		InsertStudRecord(ID, name, score, advID);	// Insert a new record
	} else {
		// Then, insert new value to the B+-tree with known block number
		studTree->insert(score, blockNum);

		// Insert a record into DB file
		StudentRecord record(ID, name, score, advID);
		Stud_DataBase.seekp(blockNum * bs + offset * sizeof(StudentRecord));
		Stud_DataBase.write(reinterpret_cast<const char*>(&record), sizeof(StudentRecord));
	}
}

void DBManager::UpdateStud(unsigned oldBlockNum) {
	StudentRecord* record = new StudentRecord[bfStud];

	Stud_DataBase.seekg(oldBlockNum * bs);

	// Read all records from an overflowed block
	Stud_DataBase.read((char*)record, sizeof(StudentRecord) * bfStud);

	char c[4096] = { 0 };
	// Erase the overlowed block
	Stud_DataBase.seekp(oldBlockNum * bs);
	Stud_DataBase.write(c, bs);

	// Allocate space for a new block
	Stud_DataBase.seekp(0, ios_base::end);
	Stud_DataBase.write(c, bs);

	// Insert records to new blocks
	for (unsigned i = 0; i < bfStud; i++) {
		unsigned offset = 0;	// position of a recond in a block (to speed-up inserting)

		unsigned blockNum = studHash->Hashing(record[i].ID, NULL, &offset);	// insert a key value into the hash table and return a block number
		studTree->update(record[i].score, oldBlockNum, blockNum);			// then, update the value in the B+-tree with known block number

		// Insert a record into DB file
		StudentRecord recordToInsert(record[i].ID, record[i].name, record[i].score, record[i].advID);
		Stud_DataBase.seekp(blockNum * bs + offset * sizeof(StudentRecord));
		Stud_DataBase.write(reinterpret_cast<const char*>(&recordToInsert), sizeof(StudentRecord));
	}

	delete[] record;
}

void DBManager::InstantiateProfessorDB(string hashfile, string salary, unsigned n) {
	profHash = new Hash((n / bfProf) * 2, hashfile, bfProf); // Suppose that the most worst utilization of a block is 1/2
	profTree = new BpTree<int>(salary);
}

void DBManager::InsertProfRecord(unsigned ID, string name, int salary) {
	bool update = false;	// whether need to read and insert again records from an overflowed block or not
	unsigned offset = 0;	// position of a recond in a block (to speed-up inserting)
	unsigned blockNum = profHash->Hashing(ID, &update, &offset);	// insert a key value into tha hash table and return a block number

	if (update) {
		UpdateProf(blockNum);					// Re-insert records from an overflowed block
		InsertProfRecord(ID, name, salary);		// Insert a new record
	}
	else {
		// Then, insert new value to the B+-tree with known block number
		profTree->insert(salary, blockNum);

		// Insert a record into DB file
		ProfessorRecord record(ID, name, salary);
		Prof_DataBase.seekp(blockNum * bs + offset * sizeof(ProfessorRecord));
		Prof_DataBase.write(reinterpret_cast<const char*>(&record), sizeof(ProfessorRecord));
	}
}

void DBManager::UpdateProf(unsigned oldBlockNum) {
	ProfessorRecord* record = new ProfessorRecord[bfProf];

	Prof_DataBase.seekg(oldBlockNum * bs);

	// Read all records from an overflowed block
	Prof_DataBase.read((char*)record, sizeof(ProfessorRecord) * bfProf);

	char c[4096] = { 0 };
	// Erase the overlowed block
	Prof_DataBase.seekp(oldBlockNum * bs);
	Prof_DataBase.write(c, bs);

	// Allocate space for a new block
	Prof_DataBase.seekp(0, ios_base::end);
	Prof_DataBase.write(c, bs);

	// Insert records to new blocks
	for (unsigned i = 0; i < bfProf; i++) {
		unsigned offset = 0;	// position of a recond in a block (to speed-up inserting)

		unsigned blockNum = profHash->Hashing(record[i].ID, NULL, &offset);	// insert a key value into the hash table and return a block number
		profTree->update(record[i].salary, oldBlockNum, blockNum);		    // then, update the value in the B+-tree with known block number

		// Insert a record into DB file
		ProfessorRecord recordToInsert(record[i].ID, record[i].name, record[i].salary);
		Prof_DataBase.seekp(blockNum * bs + offset * sizeof(ProfessorRecord));
		Prof_DataBase.write(reinterpret_cast<const char*>(&recordToInsert), sizeof(ProfessorRecord));
	}

	delete[] record;
}

void DBManager::PrintHashTable() {
	profHash->PrintTable();
}

void DBManager::PrintKthTreeNode(char table, unsigned k) {
	if (table == 's')
		studTree->searchNode(k);
	else if (table == 'p')
		profTree->searchNode(k);
}

void DBManager::PrintBlock(ProfessorRecord * record) {
	for (unsigned i = 0; i < bfProf; i++)
		if (strcmp(record[i].name, ""))
			cout << i << ": " << record[i].ID << " " << record[i].name << endl;
		else
			break;

	cout << endl;
}

void DBManager::PrintAll() {
	ProfessorRecord* record = new ProfessorRecord[bfProf];

	for (unsigned i = 0; i < 128; i++) {
		Prof_DataBase.seekg(bs * i);
		cout << "Block #" << i << endl;
		Prof_DataBase.read((char*)record, sizeof(ProfessorRecord) * bfProf);
		PrintBlock(record);
	}

	delete[] record;
}

void DBManager::ExecuteExactQuery(Table table, Attribute attr, unsigned key) {
	unsigned blNum;
	unsigned tupleNum = 0;
	StudentRecord* Srecord;
	ProfessorRecord* Precord;

	// reserve one line for storing result of a query
	Query.seekp(0, ios_base::end);
	streamoff pos = Query.tellp();
	Query.seekp(pos + 1);
	Query.write("\n", 1);

	if (table == stud) {
		Srecord = new StudentRecord[bfStud];
		blNum = studHash->FindBlock(key);
		ReadBlock(blNum, Srecord);

		for (unsigned i = 0; i < bfStud; i++)
			if (Srecord[i].ID == key) {
				Query << Srecord[i].ID << " " << Srecord[i].name << " " << Srecord[i].score << " " << Srecord[i].advID << endl;
				tupleNum++;
				delete[] Srecord;
				break;	// suppose the only one record satisfies exact search 
			}
	}
	else {
		Precord = new ProfessorRecord[bfProf];
		blNum = profHash->FindBlock(key);
		ReadBlock(blNum, Precord);

		for (unsigned i = 0; i < bfProf; i++)
			if (Precord[i].ID == key) {
				Query << Precord[i].ID << " " << Precord[i].name << " " << Precord[i].salary << endl;
				tupleNum++;
				delete[] Precord;
				break;	// suppose the only one record satisfies exact search 
			}
	}

	Query.seekp(pos);
	Query << tupleNum;
}

void DBManager::ExecuteRangeQuery(Table table, Attribute attr, int iMin, int iMax, float fMin, float fMax) {
	set<unsigned> bNumList;		// list of block numbers that has to be scanned
	set<unsigned>::iterator blNum;
	unsigned tupleNum = 0;
	StudentRecord* Srecord;
	ProfessorRecord* Precord;

	// reserve one line for storing result of a query
	Query.seekp(0, ios_base::end);
	streamoff pos = Query.tellp();
	Query.seekp(pos + sizeof(char[5]));
	Query.write("\n", 1);

	if (table == stud) {
		Srecord = new StudentRecord[bfStud];
		bNumList = studTree->findBlockList(fMin, fMax);
		//cout << "Number of blocks to be scanned: " << bNumList.size() << endl;
		for (blNum = bNumList.begin(); blNum!= bNumList.end(); ++blNum) {
			ReadBlock(*blNum, Srecord);

			for (unsigned i = 0; i < bfStud; i++)
				if (Srecord[i].score >= fMin && Srecord[i].score <= fMax) {
					Query << Srecord[i].ID << " " << Srecord[i].name << " " << Srecord[i].score << " " << Srecord[i].advID << endl;
					tupleNum++;
				}
		}
		delete[] Srecord;
	}
	else {
		Precord = new ProfessorRecord[bfProf];
		bNumList = profTree->findBlockList(iMin, iMax);
		//cout << "Number of blocks to be scanned: " << bNumList.size() << endl;
		for (blNum = bNumList.begin(); blNum != bNumList.end(); ++blNum) {
			ReadBlock(*blNum, Precord);

			for (unsigned i = 0; i < bfProf; i++)
				if (Precord[i].salary >= iMin && Precord[i].salary <= iMax) {
					Query << Precord[i].ID << " " << Precord[i].name << " " << Precord[i].salary << endl;
					tupleNum++;
				}
		}
		delete[] Precord;
	}

	Query.seekp(pos);
	Query << tupleNum;
}

// Block nested join
void DBManager::ExecuteJoin() {
	unsigned tupleNum = 0;									// number of records after execution of join
	unsigned sBl = 0, pBl = 0;								// number of disk blocks in each DB file
	StudentRecord* Srecord = new StudentRecord[bfStud];
	ProfessorRecord* Precord = new ProfessorRecord[bfProf];

	// determine number of blocks for each DB file
	Stud_DataBase.seekg(0, ios_base::end);
	sBl = Stud_DataBase.tellg() / bs;
	Prof_DataBase.seekg(0, ios_base::end);
	pBl = Prof_DataBase.tellg() / bs;

	// reserve one line for storing result of a query
	Query.seekp(0, ios_base::end);
	streamoff pos = Query.tellp();
	Query.seekp(pos + sizeof(char[7]));
	Query.write("\n", 1);

	// iterate through blocks and compare values
	for (unsigned out = 0; out < sBl; out++) {
		ReadBlock(out, Srecord);
		for (unsigned in = 0; in < pBl; in++) {
			ReadBlock(in, Precord);
			for (unsigned i = 0; i < bfStud && strcmp(Srecord[i].name, ""); i++)
				for (unsigned j = 0; j < bfProf && strcmp(Precord[j].name, ""); j++)
					if (Srecord[i].advID == Precord[j].ID) {
						Query << Srecord[i].name << "\t" << Srecord[i].ID << "\t" << Srecord[i].score << "\t"
							<< Srecord[i].advID << "\t" << Precord[j].name << "\t" << Precord[j].salary << endl;
						tupleNum++;
					}
		}
	}

	Query.seekp(pos);
	Query << tupleNum;

	delete[] Srecord, Precord;
}

template<typename Type>
void DBManager::ReadBlock(unsigned blNum, Type* record) {
	if (typeid(*record) == typeid(StudentRecord)) {
		Stud_DataBase.seekg(bs * blNum);
		Stud_DataBase.read((char*)record, sizeof(StudentRecord) * bfStud);
	}
	else {
		Prof_DataBase.seekg(bs * blNum);
		Prof_DataBase.read((char*)record, sizeof(ProfessorRecord) * bfProf);
	}
}
