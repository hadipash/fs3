#include "B+-tree.h"

template class BpTree<int>;
template class BpTree<float>;

template <class T>
BpTree<T>::BpTree(string file) {
	root = NULL;
	depth = 0;
	indexFile.open(file, ios_base::out | ios_base::binary);
}

template <class T>
BpTree<T>::~BpTree() {
	if (root != NULL) {
		SaveIntoFile(root);
		DeleteTree(root);
	}
	indexFile.close();
}

template<class T>
void BpTree<T>::DeleteTree(BpTreeNode<T>* x) {
	for (int i = 0; i < x->count; i++) {
		if (x->sNode[i] != NULL)
			if (x->sNode[i]->leaf)
				delete x->sNode[i];
			else
				DeleteTree(x->sNode[i]);
	}

	delete x;
}

template <class T>
void BpTree<T>::arrangePath()
{
	int i, j, middleBNum;
	T middleKey;
	BpTreeNode<T> *nParent, *nChild;
	BpTreeNode<T>* x = root;

	i = -1;
	while (depth != i && x != NULL) {
		if (i == -1 && x->count == maxNum) { // root node is full;
			middleKey = x->key[minNum];
			middleBNum = x->bNum[minNum];
			nChild = new BpTreeNode<T>;
			nParent = new BpTreeNode<T>;
			splitNode(x, nChild);

			nParent->leaf = false;
			nParent->key[0] = middleKey;
			nParent->bNum[0] = middleBNum;
			nParent->sNode[0] = x;
			nParent->sNode[1] = nChild;
			nParent->count++;

			root = nParent;
			depth++;
		}
		else if (i != -1 && x->count == maxNum) {
			middleKey = x->key[minNum];
			middleBNum = x->bNum[minNum];
			nChild = new BpTreeNode<T>;
			splitNode(x, nChild);

			nParent = root;
			for (j = 0; j < i; j++) {
				nParent = nParent->sNode[path[j]];
			}
			nParent->sNode[nParent->count + 1] = nParent->sNode[nParent->count];
			for (j = nParent->count; j > path[i]; j--) {
				nParent->key[j] = nParent->key[j - 1];
				nParent->bNum[j] = nParent->bNum[j - 1];
				//nParent->sNode[j + 1] = nParent->sNode[j];
				nParent->sNode[j] = nParent->sNode[j - 1];
			}
			nParent->leaf = false;
			nParent->key[path[i]] = middleKey;
			nParent->bNum[path[i]] = middleBNum;
			nParent->sNode[path[i]] = x;
			nParent->sNode[path[i] + 1] = nChild;
			nParent->count++;
		}
		i++;
		if (path[i] != -1)
			x = x->sNode[path[i]];
	}
}

template <class T>
void BpTree<T>::splitNode(BpTreeNode<T>* x, BpTreeNode<T>* y) {
	y->leaf = x->leaf;
	y->sNode[0] = x->sNode[minNum];
	y->next = x->next;
	for (int i = maxNum - 1; i >= minNum; i--) {
		y->key[i - minNum] = x->key[i];
		y->bNum[i - minNum] = x->bNum[i];
		y->sNode[i - minNum + 1] = x->sNode[i + 1];
		x->key[i] = 0;
		x->bNum[i] = -1;
		x->sNode[i + 1] = NULL;
		x->count--;
		y->count++;
	}

	x->next = y;
}

template <class T>
void BpTree<T>::insert(T a, unsigned b) {
	int i, j = 0;
	BpTreeNode<T>* x;

	x = root;
	if (x == NULL) {
		x = new BpTreeNode<T>;
		x->key[0] = a;
		x->bNum[0] = b;
		x->count++;
		root = x;
	}
	else {
		for (i = 0; i < 4; i++) {
			path[i] = -1;
		}

		// find path to insert
		findPath(a);

		// split nodes in path
		arrangePath();

		// go through the path and insert data
		x = root;
		if (x->leaf) {
			x->key[x->count] = a;
			x->bNum[x->count] = b;
			x->sort();
			x->count++;
		}
		else {
			while (!x->leaf) {
				if (a < x->key[0])
					x = x->sNode[0];
				else if (a >= x->key[x->count - 1])
					x = x->sNode[x->count];
				else {
					for (i = 0; i < x->count - 1; i++) {
						if ((a >= x->key[i]) && (a < x->key[i + 1])) {
							x = x->sNode[i + 1];
							break;
						}
					}
				}
			}
			x->key[x->count] = a;
			x->bNum[x->count] = b;
			x->sort();
			x->count++;
		}
	}
}

template <class T>
void BpTree<T>::findPath(T a) {
	int i, j = 0, k = 0;
	BpTreeNode<T>* x = root;

	while (!x->leaf) {
		if (a < x->key[0]) {
			path[j++] = 0;
			x = x->sNode[0];
		}
		else if (a >= x->key[x->count - 1]) {
			path[j++] = x->count;
			x = x->sNode[x->count];

		}
		else {
			for (i = 0; i < x->count - 1; i++) {
				if ((a >= x->key[i]) && (a < x->key[i + 1])) {
					path[j++] = i + 1;
					x = x->sNode[i + 1];
					break;
				}
			}
		}
		k++;
	}

}

template <class T>
void BpTree<T>::update(T a, unsigned oldBnum, unsigned newBNum) {
	int i;
	BpTreeNode<T>* x = root;

	if (x == NULL)
		return;
	else {
		while (x->leaf == false) {						// find leafnode
			if (a < x->key[0])
				x = x->sNode[0];
			else if (a >= x->key[x->count - 1])
				x = x->sNode[x->count];
			else {
				for (i = 0; i < (x->count - 1); i++) {
					if ((a >= x->key[i]) && (a < x->key[i + 1])) {
						x = x->sNode[i + 1];
						break;
					}
				}
			}
		}

		for (i = 0; i < x->count; i++) {
			if ((a == x->key[i]) && (oldBnum == x->bNum[i])) {
				x->bNum[i] = newBNum;
				break;
			}
		}
	}
}

template <class T>
BpTreeNode<T>* BpTree<T>::searchNode(int k) {
	int i;
	BpTreeNode<T>* x = root;

	// find fisrt leaf node
	while (!x->leaf) {
		x = x->sNode[0];
	}

	// move to k-th leaf node
	for (i = 0; i < k; i++) {
		if (x->next == NULL)
			return NULL;
		x = x->next;
	}

	cout << "Number Of " << k << "th node entries:" << x->count << endl;
	for (i = 0; i < x->count; i++) {
		cout << "Key: " << x->key[i] << "\tValue: " << x->bNum[i] << endl;
	}

	return x;
}

template <class T>
void BpTree<T>::SaveIntoFile(BpTreeNode<T>* x) {
	for (int i = 0; i < x->count; i++) {
		if (x->sNode[i] != NULL)
			if (x->sNode[i]->leaf)
				indexFile.write(reinterpret_cast<const char*>(x->sNode[i]), sizeof(BpTreeNode<T>));
			else
				SaveIntoFile(x->sNode[i]);
	}

	indexFile.write(reinterpret_cast<const char*>(x), sizeof(BpTreeNode<T>));
}

template <class T>
set<unsigned> BpTree<T>::findBlockList(T min, T max) {
	set<unsigned> bList;
	BpTreeNode<T>* x;
	
	x = root;
	if (x != NULL) {
		while (!x->leaf) {
			if (min < x->key[0])
				x = x->sNode[0];
			else if (min >= x->key[x->count - 1])
				x = x->sNode[x->count];
			else
				for (int i = 0; i < x->count - 1; i++)
					if ((min >= x->key[i]) && (min < x->key[i + 1])) {
						x = x->sNode[i + 1];
						break;
					}
		}

		int j = 0;
		while (x->key[j] <= max) {
			if (x->key[j] >= min)
				bList.insert(x->bNum[j]);
			
			j++;
			
			if (j >= x->count && x->next != NULL) {
				x = x->next;
				j = 0;
			}
		}
	}

	return bList;
}

