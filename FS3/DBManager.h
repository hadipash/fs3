#pragma once
#include <iostream>
#include "hash.h"
#include "B+-tree.h"

using namespace std;

enum Table		{ stud, prof };
enum Attribute	{ studID, profID, score, salary };

struct StudentRecord {
	unsigned ID;
	char name[20];
	float score;
	unsigned advID;

	// Constructor for reading records from a binary file (DB)
	StudentRecord()
		: ID(0), score(0), advID(0) {
		strcpy_s(name, "\0");
	}

	// Constructor for reading records from a txt file
	StudentRecord(unsigned stID, string stName, float sc, unsigned aID)
		: ID(stID), score(sc), advID(aID) {
		size_t length = stName.copy(name, 20);
		if (length < 20)
			name[length] = '\0';
	}
};

struct ProfessorRecord {
	unsigned ID;
	char name[20];
	int salary;

	// Constructor for reading records from a binary file (DB)
	ProfessorRecord()
		: ID(0), salary(0) {
		strcpy_s(name, "\0");
	}

	// Constructor for reading records from a txt file
	ProfessorRecord(unsigned profID, string prName, int s)
		: ID(profID), salary(s) {
		size_t length = prName.copy(name, 20);
		if (length < 20)
			name[length] = '\0';
	}
};

class DBManager {
private:
	fstream Stud_DataBase;
	fstream Prof_DataBase;
	ofstream Query;

	const unsigned bs = 4096;								// size of a block in bytes
	const unsigned bfStud = bs / sizeof(StudentRecord);		// blocking factor of Student record
	const unsigned bfProf = bs / sizeof(ProfessorRecord);	// blocking factor of Professor record

	Hash* studHash;
	BpTree<float>* studTree;

	Hash* profHash;
	BpTree<int>* profTree;

public:
	DBManager(string sdb, string pdb, string q);
	~DBManager();

	void InstantiateStudentDB(string hashfile, string score, unsigned n);
	void InsertStudRecord(unsigned ID, string name, float score, unsigned advID);
	void UpdateStud(unsigned oldBlockNum);

	void InstantiateProfessorDB(string hashfile, string salary, unsigned n);
	void InsertProfRecord(unsigned ID, string name, int salary);
	void UpdateProf(unsigned oldBlockNum);

	void PrintHashTable();
	void PrintKthTreeNode(char table, unsigned k);
	void PrintBlock(ProfessorRecord* record);
	void PrintAll();

	void ExecuteExactQuery(Table table, Attribute attr, unsigned key);
	void ExecuteRangeQuery(Table table, Attribute attr, int iMin, int iMax, float fMin, float fMax);
	void ExecuteJoin();

	template<typename Type>
	void ReadBlock(unsigned blNum, Type* record);
};
