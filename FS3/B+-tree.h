#pragma once
#include <iostream>
#include <fstream>
#include <set>
using namespace std;

const unsigned maxNum = (4096 / 16) - 2; //254
const unsigned minNum = maxNum / 2;

template<class T>
struct BpTreeNode {
	int count;
	bool leaf;
	BpTreeNode<T>* sNode[maxNum + 1];	// pointer to node for smaller value (compared to each entry)
	int bNum[maxNum];					// block number
	T key[maxNum];
	BpTreeNode<T>* next;

	BpTreeNode() {
		count = 0;
		leaf = true;
		next = NULL;
		sNode[maxNum] = NULL;
		for (int i = 0; i < maxNum; i++) {
			sNode[i] = NULL;
			key[i] = 0;
			bNum[i] = -1;
		}
	}

	void sort() {
		int i, j, bTemp;
		T kTemp;
		for (i = 0; i < count; i++)
			for (j = i; j < count + 1; j++)
				if (key[i] > key[j]) {
					kTemp = key[i];
					bTemp = bNum[i];
					key[i] = key[j];
					bNum[i] = bNum[j];
					key[j] = kTemp;
					bNum[j] = bTemp;
				}
	}
};

template <class T>
class BpTree {
private:
	BpTreeNode<T>* root;
	ofstream indexFile;
	int depth;
	int path[4];
	void arrangePath();
	void SaveIntoFile(BpTreeNode<T>* root);
	void splitNode(BpTreeNode<T>* x, BpTreeNode<T>* y);
	void findPath(T a);
	void DeleteTree(BpTreeNode<T>* a);

public:
	BpTree(string file);
	~BpTree();
	void insert(T data, unsigned bNum);
	void update(T data, unsigned oldBNum, unsigned newBNum);
	BpTreeNode<T>* searchNode(int k);
	set<unsigned> findBlockList(T min, T max);
};
